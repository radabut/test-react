import React from 'react';
import Member from './component/Member'
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';

function App() {
  return (
    <Router>
      <div>

        <Route path="/" exact component={Member} />

      </div>
    </Router>
  );
}

export default App;
