import React, { Component } from 'react'
import '../App.css'

export default class Member extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: ''
        }
    }

    fname = (e) => {
        this.setState({
            name: e.target.value
        })
    }
    femail = (e) => {
        this.setState({
            email: e.target.value
        })
    }

    submit = () => {
        console.log(this.state.name + ' ' + this.state.email)
    }

    render(){
        return(
            <div className="App">
                <h1>Hello Word</h1>
                <input type="text" onChange={this.fname} /><br/><br/>
                <input type="text" onChange={this.femail} /><br/><br/>
                <button onClick={this.submit}>Submit</button>
            </div>
        )
    }
}